const createServer = require("./create_server.js");
const fs = require('fs');

const get = (request, response) => {
  
  switch(request.path){
    case('/'):
    response.send(
      "200",
      { "Content-Type": "text/html" },
      fs.readFileSync("./index.html")
    );
      break;
    case'/words':
    const { words } = JSON.parse(fs.readFileSync("./words.json"));
    response.send(
      "200",
      { "Content-Type":  'application/json'},
      JSON.stringify({ "words": words })
    );

      break;
    default:
      console.log("adios");
      break;
  }
};

const post = (request, response) => {
  switch(request.path){

    case('/addWord'):
      const word = request.body;
      fs.writeFile('./words.json',word)
      response.send(
        "200",
        {'Content-Type':  'application/json'},
        "saved"
        );
    break;
    default:
      response.send(
        200,
          {
            "Content-Type": "application/json",
          },
          request.body
      );
      break;
  }

  
};

const requestListener = (request, response) => {
  switch (request.method) {
    case "GET": {
      return get(request, response);
    }
    case "POST": {
      return post(request, response);
    }
    default: {
      return response.send(
        404,
        { "Content-Type": "text/plain" },
        "The server only supports HTTP methods GET and POST"
      );
    }
  }
};

const server = createServer((request, response) => {
  try {
    return requestListener(request, response);
  } catch (error) {
    console.error(error);
    response.send(500, { "Content-Type": "text/plain" }, "Uncaught error");
  }
});

server.listen(8080);
